import {Component, OnDestroy, OnInit} from '@angular/core';
import {MoviesSearchService} from '../../services/movies-search.service';

@Component({
  selector: 'app-movies-search',
  templateUrl: './movies-search.component.html',
  styleUrls: ['./movies-search.component.scss']
})
export class MoviesSearchComponent implements OnInit {

  searchString: string;

  constructor( private moviesSearchService: MoviesSearchService) { }

  ngOnInit() {}

  onChangeSearchString() {
    localStorage.setItem('searchMovieValue', this.searchString);
    this.moviesSearchService.setMovieSearchString('search string changed');

    if (this.searchString === '') {
      localStorage.removeItem('searchMovieValue');
    }
  }

}
