import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './header/navigation/navigation.component';
import { MoviesSearchComponent } from './header/movies-search/movies-search.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { MovieItemComponent } from './movies-list/movie-item/movie-item.component';
import { BuyTicketsComponent } from './buy-tickets/buy-tickets.component';
import {MoviesStorageService} from './services/movies-storage.service';
import {HttpClientModule} from '@angular/common/http';
import { MovieDescriptionComponent } from './buy-tickets/movie-description/movie-description.component';
import { PlacesSchemaComponent } from './buy-tickets/places-schema/places-schema.component';
import {FormsModule} from '@angular/forms';
import {MoviesSearchService} from './services/movies-search.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    MoviesSearchComponent,
    MoviesListComponent,
    MovieItemComponent,
    BuyTicketsComponent,
    MovieDescriptionComponent,
    PlacesSchemaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    HttpClientModule,
    FormsModule
  ],
  providers: [
    MoviesStorageService,
    MoviesSearchService,],
  bootstrap: [AppComponent]
})
export class AppModule { }
