import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MoviesListComponent} from './movies-list/movies-list.component';
import {BuyTicketsComponent} from './buy-tickets/buy-tickets.component';

const routes: Routes = [
  {path: '', component: MoviesListComponent},
  {path: 'buy-tickets', component: BuyTicketsComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
