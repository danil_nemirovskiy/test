import { Component, OnInit } from '@angular/core';
import {Movie} from '../../models/movie.model';

@Component({
  selector: 'app-movie-description',
  templateUrl: './movie-description.component.html',
  styleUrls: ['./movie-description.component.scss']
})
export class MovieDescriptionComponent implements OnInit {

  selectedMovie: Movie;
  movieDescriptionTitles = [];

  constructor() { }

  ngOnInit() {
    this.selectedMovie = JSON.parse(localStorage.getItem('selectedMovie'));

    this.movieDescriptionTitles = [
      {
        label: 'Movie title: ',
        value: this.selectedMovie.title
      },
      {
        label: 'Description: ',
        value: this.selectedMovie.description
      },
      {
        label: 'Genre: ',
        value: this.selectedMovie.genre
      },
      {
        label: 'Start time: ',
        value: this.selectedMovie.seance.startTime
      },
      {
        label: 'Duration: ',
        value: this.selectedMovie.duration
      },
      {
        label: 'Ticket price: ',
        value: this.selectedMovie.seance.ticketPrice
      },
    ];
  }

}
