import {Component, OnInit} from '@angular/core';
import {Place} from './models/place.model';
import {Movie} from '../../models/movie.model';

@Component({
  selector: 'app-places-schema',
  templateUrl: './places-schema.component.html',
  styleUrls: ['./places-schema.component.scss']
})
export class PlacesSchemaComponent implements OnInit {

  placesSchema: Place[][] = [];
  selectedPlace: Place;
  selectedMovie: Movie;

  constructor() {}

  ngOnInit() {
    this.selectedMovie = JSON.parse(localStorage.getItem('selectedMovie'));
    this.placesSchema = this.selectedMovie.seance.placesSchema;
  }


  onBuyTicket(place) {
    this.selectedPlace = place;
    this.selectedPlace.status = this.selectedPlace.type === 'place' ? 'busy' : '';
    localStorage.setItem('selectedMovie', JSON.stringify(this.selectedMovie));
  }

}
