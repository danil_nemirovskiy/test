import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

@Injectable()

export class MoviesStorageService {

  constructor(private http: HttpClient) {}

  onGetMovies() {
    return this.http.get('./assets/movies.json');
  }
}

