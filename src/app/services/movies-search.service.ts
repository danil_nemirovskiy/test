import {BehaviorSubject} from 'rxjs/index';

export class MoviesSearchService {

  movieSearchStringChanged = new BehaviorSubject<string>('');
  movieSearchString: string;

  setMovieSearchString(changeEvent) {
    this.movieSearchStringChanged.next(changeEvent);
  }

}
