import {Place} from '../buy-tickets/places-schema/models/place.model';

export class Movie {
  public title: string;
  public description: string;
  public genre: string;
  public duration: string;
  public dateOfRelease: string;
  public seance: {
    startTime: string,
    ticketPrice: string,
    placesSchema: Place[][]
  };
}
