import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {MoviesStorageService} from '../services/movies-storage.service';
import {Subscription} from 'rxjs/index';
import {Movie} from '../models/movie.model';
import {MoviesSearchService} from '../services/movies-search.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit, OnDestroy {

  movies: Movie[];
  subs: Subscription[] = [];
  moviesClone: Movie[];

  constructor(private moviesStorageService: MoviesStorageService,
              private moviesSearchService: MoviesSearchService) {
  }

  ngOnInit() {
    this.subs.push(
      this.moviesStorageService.onGetMovies().subscribe(
        (moviesData: { movies: Movie[] }) => {
          this.movies = moviesData.movies;
          this.moviesClone = this.movies;
        }
      ),


      this.moviesSearchService.movieSearchStringChanged.subscribe(
        () => {
          const searchMovieValue = localStorage.getItem('searchMovieValue');

          if (typeof this.movies !== 'undefined' && searchMovieValue !== null) {
            this.movies = this.moviesClone.filter((movie: Movie) => {
              return movie.title.toLowerCase().search(searchMovieValue.toLowerCase()) !== -1;
            });
          }
        }
      )
    );
  }

  ngOnDestroy() {
    this.subs.forEach((s: Subscription) => s.unsubscribe());
  }

}
